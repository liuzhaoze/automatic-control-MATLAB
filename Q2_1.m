num1 = 5 * conv(conv([1, 2], [1, 2]), [1, 6, 7]);
den1 = conv(conv(conv(conv([1, 0], [1, 1]), [1, 1]), [1, 1]), [1, 0, 2, 1]);
sys1 = tf(num1, den1);
printsys(num1, den1);

num2 = 5;
den2 = conv(conv([1, 0], [1, 1]), [1, 4, 4]);
sys2 = tf(num2, den2);
printsys(num2, den2);