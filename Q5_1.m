num = [4, 1];
den = conv(conv(conv([1, 0], [1, 0]), [1, 1]), [2, 1]);
nyquist(num, den);
axis([-30, 5, -4, 4]);
% 该开环系统的奈氏曲线顺时针包围点(-1, j0)
% 则该闭环系统不稳定