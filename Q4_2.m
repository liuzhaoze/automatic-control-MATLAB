k = 1; z = []; p = [0, -1];
G = zpk(z, p, k);
k = 1; z = -2; p = [0, -1];
G1 = zpk(z, p, k);

rlocus(G, G1);
legend('G(s)', 'G1(s)', 'Location', 'northwest');

sys = feedback(G, 1);
sys1 = feedback(G1, 1);
figure;
step(sys, sys1);
legend('G(s)', 'G1(s)', 'Location', 'southeast');