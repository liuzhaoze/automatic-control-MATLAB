num3 = 1.05;
den3 = conv(conv([0.125, 1], [0.5, 1]), [1, 1, 1]);
phi3 = tf(num3, den3);

num4 = conv(1.05, [1, 1]);
den4 = conv(conv([0.125, 1], [0.5, 1]), [1, 1, 1]);
phi4 = tf(num4, den4);

ax1 = subplot(2, 2, [1, 2]);
ax2 = subplot(2, 2, 3);
ax3 = subplot(2, 2, 4);

step(ax1, phi3, phi4);
grid on;
legend(ax1, {'\Phi3(s)', '\Phi4(s)'});

pzmap(ax2, phi3);
title(ax2, '\Phi_3的零极点图');
axis(ax2, [-9, 0, -1, 1]);

pzmap(ax3, phi4);
title(ax3, '\Phi_4的零极点图');
axis(ax3, [-9, 0, -1, 1]);