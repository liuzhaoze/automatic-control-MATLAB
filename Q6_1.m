num = 20;
den = conv([1, 0], [1, 2]);
G0 = tf(num, den); % 未校正系统的开环传递函数
[Gm, Pm, Wcg, Wcp] = margin(G0); % 未校正系统的频率性能指标

w = 0.1:0.1:10000; % 确定采样点
[mag, phase, wout] = bode(G0, w); % 获得bode图
magdb = 20 * log10(mag); % 计算对数幅频响应

phim1 = 35; delta = 8; % 设置目标相位裕度参数
phim = phim1 - Pm + delta; % 确定校正装置提供的相位超前量
beta = (1 - sin(phim * pi / 180)) / (1 + sin(phim * pi / 180));
n = find(magdb + 10 * log10(1 / beta) <= 0.0001); % 找到满足该式的magdb向量的所有下标值
wc = n(1);
w1 = (wc / 10) * sqrt(beta);
w2 = (wc / 10) / sqrt(beta); % 确定校正装置的两个转折频率

numc = [1/w1, 1];
denc = [1/w2, 1];
Gc = tf(numc, denc); % 校正装置的传递函数
G = Gc * G0; % 矫正后系统的传递函数
bode(G0, G); % 校正前后系统的Bode图
hold on;
margin(G); % 在同一窗口显示校正后的频率指标

[Gmc, Pmc, Wcgc, Wcpc] = margin(G); % 校正后系统的频率性能指标
Gmcdb = 20 * log10(Gmc);

disp('校正装置的传递函数：');
Gc
disp('矫正后系统的开环传递函数：');
G
disp('矫正后系统的频率性能指标：');
[Gmcdb, Pmc, Wcgc, Wcpc]
disp('校正装置的参数T和值：');
[1/w1, beta]