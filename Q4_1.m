k = 1; z = []; p = [0, -1, -2];
[num, den] = zp2tf(z, p, k);
rlocus(num, den);
sgrid(0:0.05:1, 0:1:10);
%%
[k, r] = rlocfind(num, den)
GH = zpk([], r, k);    % 开环传递函数
sys = feedback(GH, 1); % 闭环传递函数
step(sys);