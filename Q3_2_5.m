num2 = 1.05;
den2 = [1, 1, 1];
phi2 = tf(num2, den2);

num5 = conv(1.05, [0.4762, 1]);
den5 = conv([0.5, 1], [1, 1, 1]);
phi5 = tf(num5, den5);

ax1 = subplot(2, 2, [1, 2]);
ax2 = subplot(2, 2, 3);
ax3 = subplot(2, 2, 4);

step(ax1, phi2, phi5);
grid on;
legend(ax1, {'\Phi2(s)', '\Phi5(s)'});

pzmap(ax2, phi2);
title(ax2, '\Phi_2的零极点图');
axis(ax2, [-3, 0, -1, 1]);

pzmap(ax3, phi5);
title(ax3, '\Phi_5的零极点图');
axis(ax3, [-3, 0, -1, 1]);