G = tf(10, [1, 2, 10]);
G1 = tf([2, 10], [1, 2, 10]);
G2 = tf([1, 0.5, 10], [1, 2, 10]);
G3 = tf([1, 0.5, 0], [1, 2, 10]);
G4 = tf([1, 0], [1, 2, 10]);

ax1 = subplot(2, 2, 1);
ax2 = subplot(2, 2, 2);
ax3 = subplot(2, 2, 3);
ax4 = subplot(2, 2, 4);

[y, t] = step(G, 6);
plot(ax1, t, y); hold(ax1, 'on');
plot(ax2, t, y); hold(ax2, 'on');
plot(ax3, t, y); hold(ax3, 'on');
plot(ax4, t, y); hold(ax4, 'on');

[y, t] = step(G1, 6); plot(ax1, t, y);
[y, t] = step(G2, 6); plot(ax2, t, y);
[y, t] = step(G3, 6); plot(ax3, t, y);
[y, t] = step(G4, 6); plot(ax4, t, y);

title(ax1, 'G_1(s)与G(s)对比'); grid(ax1, 'on');
title(ax2, 'G_2(s)与G(s)对比'); grid(ax2, 'on');
title(ax3, 'G_3(s)与G(s)对比'); grid(ax3, 'on');
title(ax4, 'G_4(s)与G(s)对比'); grid(ax4, 'on');

xlabel(ax1, 'Time (seconds)'); ylabel(ax1, 'Amplitude');
xlabel(ax2, 'Time (seconds)'); ylabel(ax2, 'Amplitude');
xlabel(ax3, 'Time (seconds)'); ylabel(ax3, 'Amplitude');
xlabel(ax4, 'Time (seconds)'); ylabel(ax4, 'Amplitude');

legend(ax1, {'G(s)','G_1(s)'});
legend(ax2, {'G(s)','G_2(s)'});
legend(ax3, {'G(s)','G_3(s)'});
legend(ax4, {'G(s)','G_4(s)'});