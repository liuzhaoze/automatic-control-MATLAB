den = conv(conv([1, 0], [1, 0]), [0.1, 1]);

km = 0; % 用于储存获得最大相位裕度时的增益
Pmm = 0; % 用于储存目前遇到的最大的相位裕度

for k = 3:0.0001:4
    num = k * [1, 1];
    [Gm, Pm, Wcg, Wcp] = margin(num, den); % 获取当前k值下的相位裕度
    if Pm > Pmm
        Pmm = Pm;
        km = k;
    end
end

sprintf('系统获得最大相位裕度的k值为：%.4f\n此时的相位裕度为：%.4fdeg', km, Pmm)