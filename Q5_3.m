% 改写至Q5_3_12
num = [1, 1];
den = conv(conv([1, 0], [1, 0]), [0.1, 1]);
margin(num, den);
% 系统的幅值裕度为：-infdB   小于0，系统不稳定

% 下面这个算法不对，见Q5_3_3
[Gm, Pm, Wcg, Wcp] = margin(num, den); % 获得最大相位裕度的频率值
[mag, phase, wout] = bode(num, den);   % 获得bode图的数据
M = spline(wout, mag, Wcp);            % 获得最大相位裕度频率值对应的幅值
k = 1 / M